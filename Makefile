include .env

MIGRATIONS_PATH ?= file://migrations
POSTGRESQL_URL ?= postgresql://${POSTGRES_USER}:${POSTGRES_PASSWORD}@localhost:${POSTGRES_PORT}/${POSTGRES_USER}?sslmode=disable

run:
	docker-compose up --build

migrate_up:
	migrate -source ${MIGRATIONS_PATH} \
		-database ${POSTGRESQL_URL} up 1

migrate_down:
	migrate -source ${MIGRATIONS_PATH} \
		-database ${POSTGRESQL_URL} down 1

psql:
	psql ${POSTGRESQL_URL}

swagger_ui:
	python -m webbrowser http://localhost:${HTTP_PORT}/v1alpha/swagger/index.html

.PHONY: run migrate_up migrate_down psql swagger_ui
