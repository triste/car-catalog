FROM golang:latest AS builder
WORKDIR /usr/src/car-catalog
COPY go.mod go.sum ./
RUN go mod download && go mod verify
COPY . .
RUN go build -v -o /usr/local/bin/ ./...
CMD [ "car-catalog" ]
