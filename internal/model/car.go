package model

import (
	"errors"
	"regexp"
)

type EngineType = int

type CarOwner struct {
	Name       string `json:"name"`
	Surname    string `json:"surname"`
	Patronymic string `json:"patronymic,omitempty"`
}

type Car struct {
	ID     int      `json:"id"`
	RegNum string   `json:"reg_num"`
	Mark   string   `json:"mark"`
	Model  string   `json:"model"`
	Year   int      `json:"year,omitempty"`
	Owner  CarOwner `json:"owner"`
}

func (c *Car) Validate() error {
	// Some business constraints
	r := regexp.MustCompile("[А-Я][0-9]{3}[А-Я]{2}[0-9]{2,3}")
	if !r.MatchString(c.RegNum) {
		return errors.New("registration number does not match the pattern")
	}
	if len(c.Mark) == 0 {
		return errors.New("mark lenght == 0")
	}
	if len(c.Model) == 0 {
		return errors.New("model lenght == 0")
	}
	if c.Year < 1886 {
		return errors.New("the car cannot be older than the world's first internal engine car")
	}
	if len(c.Owner.Name) == 0 {
		return errors.New("owner name lenght == 0")
	}
	if len(c.Owner.Surname) == 0 {
		return errors.New("owner surname lenght == 0")
	}
	return nil
}

type CarOwnerUpdateMask struct {
	Name       *string `json:"name"`
	Surname    *string `json:"surname"`
	Patronymic *string `json:"patronymic"`
}

type CarUpdateMask struct {
	RegNum *string             `json:"reg_num"`
	Mark   *string             `json:"mark"`
	Model  *string             `json:"model"`
	Year   *int                `json:"year"`
	Owner  *CarOwnerUpdateMask `json:"owner"`
}

type CarFilter struct {
	ID              *int    `form:"id" op:"=" db:"id"`
	RegNum          *string `form:"reg_num" op:"=" db:"reg_num"`
	Mark            *string `form:"mark" op:"=" db:"mark"`
	Model           *string `form:"model" op:"=" db:"model"`
	YearMax         *int    `form:"year_max" op:"<=" db:"year"`
	YearMin         *int    `form:"year_min" op:">=" db:"year"`
	OwnerName       *string `form:"owner_name" op:"=" db:"owner_name"`
	OwnerSurname    *string `form:"owner_surname" op:"=" db:"owner_surname"`
	OwnerPatronymic *string `form:"owner_patronymic" op:"=" db:"owner_patronymic"`
	Limit           *int    `form:"limit"`
	Offset          *int    `form:"offset"`
}
