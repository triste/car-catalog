package rest

import (
	"errors"
	"net/http"
	"strconv"
	"time"

	ginzap "github.com/gin-contrib/zap"
	"github.com/gin-gonic/gin"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
	"gitlab.com/triste/car-catalog/docs"
	"gitlab.com/triste/car-catalog/internal/model"
	"gitlab.com/triste/car-catalog/internal/service"
	"go.uber.org/zap"
)

func errorHandler(gctx *gin.Context) {
	gctx.Next()
	for _, err := range gctx.Errors {
		// TODO: aggregate errors
		var errNotFound *service.ErrNotFound
		if errors.As(err, &errNotFound) {
			NewErrResponse(gctx, http.StatusNotFound, err)
			continue
		}
		var errInvalidArg *service.ErrInvalidArgument
		if errors.As(err, &errInvalidArg) {
			NewErrResponse(gctx, http.StatusBadRequest, err)
			continue
		}
		NewErrResponse(gctx, http.StatusInternalServerError, err)
	}
}

type ErrResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func NewErrResponse(gctx *gin.Context, status int, err error) {
	gctx.IndentedJSON(status, ErrResponse{
		Code:    status,
		Message: err.Error(),
	})
}

type controller struct {
	service *service.CatalogService
}

// @title Car catalog
// @version 1.0
// @description Car catalog service
// @BasePath /v1alpha
func NewController(service *service.CatalogService) http.Handler {
	router := gin.Default()
	logger := zap.L()
	router.Use(ginzap.Ginzap(logger, time.RFC3339, true))
	router.Use(ginzap.RecoveryWithZap(logger, false))
	router.Use(errorHandler)
	ctrl := controller{
		service: service,
	}
	docs.SwaggerInfo.BasePath = "/v1alpha"
	v1alpha := router.Group("/v1alpha")
	{
		cars := v1alpha.Group("/cars")
		{
			cars.GET("", ctrl.ListCars)
			cars.POST("", ctrl.AddCars)
			cars.PATCH("/:id", ctrl.UpdateCar)
			cars.DELETE("/:id", ctrl.DeleteCar)
		}
		v1alpha.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	}
	return router
}

// @Summary List cars by filter
// @ID list-cars
// @Param request query model.CarFilter false "Query parameters"
// @Accept json
// @Produce json
// @Success 200 {array} model.Car
// @Failure 400 {object} ErrResponse
// @Router /cars [get]
func (c *controller) ListCars(gctx *gin.Context) {
	filter := model.CarFilter{}
	if err := gctx.ShouldBindQuery(&filter); err != nil {
		NewErrResponse(gctx, http.StatusBadRequest, err)
		return
	}
	ctx := gctx.Request.Context()
	cars := c.service.ListCars(ctx, filter)
	gctx.IndentedJSON(http.StatusOK, cars)
}

type EmptyResponse struct{}

// @Summary Delete car by identirier
// @ID delete-car
// @Param id path int true "Car identifier"
// @Produce json
// @Success 200 {object} EmptyResponse
// @Failure 400 {object} ErrResponse
// @Failure 404 {object} ErrResponse
// @Router /cars/{id} [delete]
func (c *controller) DeleteCar(gctx *gin.Context) {
	id, err := strconv.Atoi(gctx.Param("id"))
	if err != nil {
		NewErrResponse(gctx, http.StatusBadRequest, errors.New("invalid id"))
		return
	}
	ctx := gctx.Request.Context()
	if err := c.service.DeleteCar(ctx, id); err != nil {
		gctx.Error(err)
		return
	}
	gctx.IndentedJSON(http.StatusOK, "")
}

// @Summary Update car
// @ID update-car
// @Param id path int true "Car identifier"
// @Param update_mask body model.CarUpdateMask true "Car update mask"
// @Accept json
// @Produce json
// @Success 200 {object} model.Car
// @Failure 400 {object} ErrResponse
// @Failure 404 {object} ErrResponse
// @Router /cars/{id} [patch]
func (c *controller) UpdateCar(gctx *gin.Context) {
	updateFields := model.CarUpdateMask{}
	if err := gctx.BindJSON(&updateFields); err != nil {
		NewErrResponse(gctx, http.StatusBadRequest, err)
		return
	}
	id, err := strconv.Atoi(gctx.Param("id"))
	if err != nil {
		NewErrResponse(gctx, http.StatusBadRequest, errors.New("invalid id"))
		return
	}
	ctx := gctx.Request.Context()
	car, err := c.service.UpdateCar(ctx, id, &updateFields)
	if err != nil {
		gctx.Error(err)
		return
	}
	gctx.IndentedJSON(http.StatusOK, car)
}

type AddCarsRequest struct {
	Regnums []string `json:"regNums" binding:"required"`
}

// @Summary Add cars by registration numbers
// @ID add-cars
// @Param regnums body AddCarsRequest true "Car registration numbers"
// @Accept json
// @Produce json
// @Success 200 {array} model.Car
// @Failure 400 {object} ErrResponse
// @Router /cars [post]
func (c *controller) AddCars(gctx *gin.Context) {
	data := AddCarsRequest{}
	if err := gctx.BindJSON(&data); err != nil {
		NewErrResponse(gctx, http.StatusBadRequest, err)
		return
	}
	ctx := gctx.Request.Context()
	cars, err := c.service.AddCars(ctx, data.Regnums)
	if err != nil {
		gctx.Error(err)
		return
	}
	gctx.IndentedJSON(http.StatusOK, cars)
}
