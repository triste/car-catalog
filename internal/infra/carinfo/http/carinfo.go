package http

import (
	"context"
	"fmt"
	"net/url"

	"gitlab.com/triste/car-catalog/internal/infra/carinfo"
	"go.uber.org/zap"
)

type service struct {
	client  *ClientWithResponses
	address string
}

func (s *service) GetCarInfoByRegNum(ctx context.Context, regnum string) (*carinfo.CarInfo, error) {
	zap.L().Info("fetching car information",
		zap.String("address", s.address),
		zap.String("regnum", regnum),
	)
	getinfoParams := GetInfoParams{
		RegNum: regnum,
	}
	resp, err := s.client.GetInfoWithResponse(ctx, &getinfoParams)
	if err != nil {
		return nil, fmt.Errorf("get info: %w", err)
	}
	switch resp.StatusCode() {
	case 200:
		return &carinfo.CarInfo{
			Regnum: resp.JSON200.RegNum,
			Mark:   resp.JSON200.Mark,
			Model:  resp.JSON200.Model,
			Year:   resp.JSON200.Year,
			Owner: carinfo.CarOwner{
				Name:       resp.JSON200.Owner.Name,
				Surname:    resp.JSON200.Owner.Surname,
				Patronymic: resp.JSON200.Owner.Patronymic,
			},
		}, nil
	case 400:
		zap.L().Error("bad request to car information service",
			zap.String("address", s.address),
			zap.String("method", "GetInfo"),
			zap.String("regnum", regnum),
		)
		return nil, nil
	case 500:
		zap.L().Warn("something wrong with car information service",
			zap.String("address", s.address),
		)
		return nil, carinfo.ErrInternal
	default:
		zap.L().Warn("car information service responds with an unknown error, the API may have changed",
			zap.String("address", s.address),
			zap.Int("code", resp.StatusCode()),
		)
		return nil, carinfo.ErrUnknown
	}
}

func NewCarInfoService(address url.URL) carinfo.CarInfoService {
	client, err := NewClientWithResponses(address.String())
	if err != nil { // actually never happens without custom client options
		zap.L().Error("http car info service: can't create client", zap.Error(err))
		return nil
	}
	return &service{
		client: client,
	}
}
