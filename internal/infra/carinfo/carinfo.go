package carinfo

import (
	"context"
	"errors"
)

type CarOwner struct {
	Name       string
	Surname    string
	Patronymic *string
}

type CarInfo struct {
	Regnum string
	Mark   string
	Model  string
	Year   *int
	Owner  CarOwner
}

var ErrInternal = errors.New("internal error in car information service")
var ErrUnknown = errors.New("car information service respond with unknown error")

type CarInfoService interface {
	GetCarInfoByRegNum(ctx context.Context, regnum string) (*CarInfo, error)
}
