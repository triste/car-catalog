package inmem

import (
	"context"

	"gitlab.com/triste/car-catalog/internal/infra/carinfo"
)

type service struct {
}

func newYearPtr(year int) *int {
	return &year
}

func newPatronymicPtr(patronymic string) *string {
	return &patronymic
}

var carinfos = map[string]*carinfo.CarInfo{
	"Х123ХХ150": {
		Regnum: "Х123ХХ150",
		Mark:   "Lada",
		Model:  "Vesta",
		Year:   newYearPtr(2002),
		Owner: carinfo.CarOwner{
			Name:       "Ivan",
			Surname:    "Ivanov",
			Patronymic: newPatronymicPtr("Ivanovich"),
		},
	},
	"А342АА777": {
		Regnum: "А342АА777",
		Mark:   "BMW",
		Model:  "X3",
		Year:   nil,
		Owner: carinfo.CarOwner{
			Name:       "Ilya",
			Surname:    "Muromets",
			Patronymic: nil,
		},
	},
}

func (s *service) GetCarInfoByRegNum(ctx context.Context, regnum string) (*carinfo.CarInfo, error) {
	if carinfo, ok := carinfos[regnum]; ok {
		return carinfo, nil
	}
	return nil, nil
}

func NewCarInfoService() carinfo.CarInfoService {
	return &service{}
}
