package app

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"sync/atomic"

	"gitlab.com/triste/car-catalog/internal/controller/rest"
	carinfo "gitlab.com/triste/car-catalog/internal/infra/carinfo"
	carinfoHttp "gitlab.com/triste/car-catalog/internal/infra/carinfo/http"
	carinfoInmem "gitlab.com/triste/car-catalog/internal/infra/carinfo/inmem"
	"gitlab.com/triste/car-catalog/internal/repo/postgres"
	"gitlab.com/triste/car-catalog/internal/service"
	"go.uber.org/zap"
)

type Application struct {
	postgres   *postgres.Postgres
	httpServer *http.Server
	started    atomic.Bool
}

func NewApplication(config Config) (*Application, error) {
	pg, err := postgres.NewPostgres(config.Postgres)
	if err != nil {
		return nil, fmt.Errorf("postgres: %w", err)
	}
	carRepo := postgres.NewCarRepository(pg)
	emptyURL := url.URL{}
	var carInfoService carinfo.CarInfoService
	if config.CarInfoURL != emptyURL {
		carInfoService = carinfoHttp.NewCarInfoService(config.CarInfoURL)
	} else {
		zap.L().Debug("using in-memory implementaion of car info service")
		carInfoService = carinfoInmem.NewCarInfoService()
	}
	service := service.NewService(carInfoService, carRepo)
	controller := rest.NewController(&service)
	address := fmt.Sprintf(":%v", config.HttpPort)
	httpServer := http.Server{
		Addr:    address,
		Handler: controller,
	}
	return &Application{
		postgres:   pg,
		httpServer: &httpServer,
	}, nil
}

func (a *Application) Start() error {
	if !a.started.CompareAndSwap(false, true) {
		return errors.New("already started")
	}
	defer a.started.Store(false)
	zap.L().Info("starting http server",
		zap.String("address", a.httpServer.Addr),
	)
	return a.httpServer.ListenAndServe()
}

func (a *Application) Stop() error {
	zap.L().Info("stopping http server",
		zap.String("address", a.httpServer.Addr),
	)
	return a.httpServer.Shutdown(context.Background())
}

func (a *Application) Close() error {
	return a.postgres.Close()
}
