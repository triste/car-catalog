package app

import (
	"net/url"

	"gitlab.com/triste/car-catalog/internal/repo/postgres"
)

type Config struct {
	HttpPort   uint            `env:"HTTP_PORT" envDefault:"8080"`
	CarInfoURL url.URL         `env:"CARINFO_URL"`
	Postgres   postgres.Config `envPrefix:"POSTGRES_"`
}
