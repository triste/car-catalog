package service

import (
	"fmt"
)

type ErrNotFound struct {
	ResourceName string
}

func (e *ErrNotFound) Error() string {
	return fmt.Sprintf("not found: %v", e.ResourceName)
}

func NewErrNotFound(res string) error {
	return &ErrNotFound{
		ResourceName: res,
	}
}

type ErrInvalidArgument struct {
	message string
}

func (e *ErrInvalidArgument) Error() string {
	return fmt.Sprintf("invalid argument: %v", e.message)
}

func NewErrInvalidArgument(err error) error {
	return &ErrInvalidArgument{
		message: err.Error(),
	}
}
