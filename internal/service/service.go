package service

import (
	"context"

	"gitlab.com/triste/car-catalog/internal/infra/carinfo"
	"gitlab.com/triste/car-catalog/internal/model"
	"gitlab.com/triste/car-catalog/internal/repo"
	"go.uber.org/zap"
)

type CatalogService struct {
	carInfoService carinfo.CarInfoService
	carRepo        repo.CarRepository
}

func NewService(carinfoService carinfo.CarInfoService, carRepo repo.CarRepository) CatalogService {
	return CatalogService{
		carInfoService: carinfoService,
		carRepo:        carRepo,
	}
}

func (c *CatalogService) ListCars(ctx context.Context, filter model.CarFilter) []model.Car {
	return c.carRepo.GetByFilter(ctx, filter)
}

func carPatchByCarInfo(car *model.Car, carinfo *carinfo.CarInfo) {
	if carinfo.Year != nil {
		car.Year = *carinfo.Year
	}
	if carinfo.Owner.Patronymic != nil {
		car.Owner.Patronymic = *carinfo.Owner.Patronymic
	}
	car.RegNum = carinfo.Regnum
	car.Mark = carinfo.Mark
	car.Model = carinfo.Model
	car.Owner.Name = carinfo.Owner.Name
	car.Owner.Surname = carinfo.Owner.Surname
}

func (c *CatalogService) AddCars(ctx context.Context, regnums []string) ([]model.Car, error) {
	carInfoMap := make(map[string]*carinfo.CarInfo, len(regnums))
	for _, regnum := range regnums {
		if _, ok := carInfoMap[regnum]; ok {
			zap.L().Debug("car information already fetched",
				zap.String("regnum", regnum),
			)
			continue
		}
		zap.L().Debug("fetching car information from external service...",
			zap.String("regnum", regnum),
		)
		// TODO: deadline/timeout
		ctx := context.TODO()
		carinfo, err := c.carInfoService.GetCarInfoByRegNum(ctx, regnum)
		if err != nil {
			return nil, err
		}
		if carinfo == nil {
			return nil, NewErrNotFound(regnum)
		}
		carInfoMap[regnum] = carinfo
	}
	cars := make([]model.Car, 0, len(regnums))
	for _, regnum := range regnums {
		carinfo := carInfoMap[regnum]
		car := model.Car{}
		carPatchByCarInfo(&car, carinfo)
		cars = append(cars, car)
	}
	err := c.carRepo.Insert(ctx, cars)
	if err != nil {
		return nil, err
	}
	return cars, nil
}

func (c *CatalogService) UpdateCar(
	ctx context.Context,
	id int,
	updateFields *model.CarUpdateMask,
) (*model.Car, error) {
	// TODO: optimistic/pessemistic locking
	car := c.carRepo.GetByID(ctx, id)
	if car == nil {
		return nil, NewErrNotFound("car")
	}
	if updateFields.RegNum != nil {
		zap.L().Debug("fetching car information from external service...",
			zap.String("regnum", *updateFields.RegNum),
		)
		// TODO: deadline/timeout
		ctx := context.TODO()
		carinfo, err := c.carInfoService.GetCarInfoByRegNum(ctx, *updateFields.RegNum)
		if err != nil {
			return nil, err
		}
		if carinfo == nil {
			return nil, NewErrNotFound(*updateFields.RegNum)
		}
		carPatchByCarInfo(car, carinfo)
	}
	if updateFields.Mark != nil {
		car.Mark = *updateFields.Mark
	}
	if updateFields.Model != nil {
		car.Model = *updateFields.Model
	}
	if updateFields.Year != nil {
		car.Year = *updateFields.Year
	}
	if updateFields.Owner != nil {
		if updateFields.Owner.Name != nil {
			car.Owner.Name = *updateFields.Owner.Name
		}
		if updateFields.Owner.Surname != nil {
			car.Owner.Surname = *updateFields.Owner.Surname
		}
		if updateFields.Owner.Patronymic != nil {
			car.Owner.Patronymic = *updateFields.Owner.Patronymic
		}
	}
	if err := car.Validate(); err != nil {
		return nil, NewErrInvalidArgument(err)
	}
	if c.carRepo.Update(ctx, car) {
		return car, nil
	}
	return nil, NewErrNotFound("car")
}

func (c *CatalogService) DeleteCar(ctx context.Context, id int) error {
	if c.carRepo.Delete(ctx, id) {
		return nil
	}
	return NewErrNotFound("car")
}
