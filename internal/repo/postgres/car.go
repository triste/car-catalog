package postgres

import (
	"context"
	"errors"
	"fmt"
	"reflect"
	"strconv"
	"strings"

	"github.com/lib/pq"
	"gitlab.com/triste/car-catalog/internal/model"
	"gitlab.com/triste/car-catalog/internal/repo"
	"go.uber.org/zap"
)

type carRepo struct {
	*Postgres
}

func (r *carRepo) GetByID(ctx context.Context, id int) *model.Car {
	const query = `
	SELECT * FROM cars
	WHERE id = $1
	`
	zap.L().Debug("GetByID quering",
		zap.String("query", query),
	)
	row := r.db.QueryRowContext(ctx, query, id)
	dto := newCarDtoFromScanner(row)
	if dto == nil {
		return nil
	}
	car := dto.toModel()
	return &car
}

func (r *carRepo) GetByFilter(ctx context.Context, filter model.CarFilter) []model.Car {
	args := []any{}
	conds := []string{}
	filterVal := reflect.ValueOf(filter)
	filterType := reflect.TypeOf(filter)
	for i := range filterVal.NumField() {
		field := filterVal.Field(i)
		if field.IsNil() {
			continue
		}
		typ := filterType.Field(i)
		op := typ.Tag.Get("op")
		if len(op) == 0 {
			zap.L().Debug("field does not have op tag, skipping",
				zap.String("field", typ.Name),
			)
			continue
		}
		colname := typ.Tag.Get("db")
		if len(colname) == 0 {
			zap.L().Debug("field does not have db tag, skipping",
				zap.String("field", typ.Name),
			)
			continue
		}
		args = append(args, field.Interface())
		conds = append(conds, fmt.Sprintf("%v %v $%v", colname, op, len(args)))
	}
	query := `SELECT * FROM cars`
	if len(conds) > 0 {
		query += " WHERE " + strings.Join(conds, " AND ")
	}
	if filter.Limit != nil {
		args = append(args, *filter.Limit)
		query += fmt.Sprintf(" LIMIT $%v", len(args))
	}
	if filter.Offset != nil {
		args = append(args, *filter.Offset)
		query += fmt.Sprintf(" OFFSET $%v", len(args))
	}
	zap.L().Debug("carRepo.GetByFilter quering",
		zap.String("query", query),
	)
	rows, err := r.db.QueryContext(ctx, query, args...)
	if err != nil {
		zap.L().Error("GetByFilter query failed",
			zap.String("query", query),
			zap.Error(err),
		)
		return nil
	}
	defer rows.Close()
	cars := make([]model.Car, 0)
	for rows.Next() {
		dto := newCarDtoFromScanner(rows)
		if dto != nil {
			car := dto.toModel()
			cars = append(cars, car)
		}
	}
	return cars
}

func getDbTag(field reflect.StructField) string {
	tag := field.Tag.Get("db")
	if len(tag) == 0 {
		zap.L().Error("carDTO field has no `db` tag or is empty",
			zap.String("field", field.Name),
		)
	}
	return tag
}

func (r *carRepo) Insert(ctx context.Context, cars []model.Car) error {
	if len(cars) == 0 {
		return nil
	}
	carDtoType := reflect.TypeFor[carDTO]()
	fieldCount := reflect.Zero(carDtoType).NumField()
	columnNames := make([]string, 0, fieldCount-1) // skip id
	for i := 1; i < fieldCount; i++ {
		field := carDtoType.Field(i)
		colname := getDbTag(field)
		columnNames = append(columnNames, colname)
	}
	args := make([]any, 0, (fieldCount-1)*len(cars))
	valsets := make([]string, 0, len(cars))
	for _, car := range cars {
		dto := carToDTO(&car)
		v := reflect.ValueOf(dto)
		valset := make([]string, 0, v.NumField()-1)
		for i := 1; i < v.NumField(); i++ {
			args = append(args, v.Field(i).Interface())
			valset = append(valset, "$"+strconv.Itoa(len(args)))
		}
		valsets = append(valsets, fmt.Sprintf("(%v)", strings.Join(valset, ", ")))
	}
	query := fmt.Sprintf(
		"INSERT INTO cars(%v) VALUES %v RETURNING id",
		strings.Join(columnNames, ", "),
		strings.Join(valsets, ", "),
	)
	zap.L().Debug("carRepo.Insert quering",
		zap.String("query", query),
	)
	rows, err := r.db.QueryContext(ctx, query, args...)
	var pqErr *pq.Error
	if err != nil {
		if errors.As(err, &pqErr) {
			if pqErr.Constraint == "cars_unique_reg_num" {
				return errors.New(pqErr.Detail)
			} else {
				zap.L().Warn("unexpected postgres response",
					zap.Any("response", pqErr),
				)
			}
		} else {
			zap.L().Error("unknown query error",
				zap.Error(err),
			)
		}
		return nil
	}
	defer rows.Close()
	for i := 0; rows.Next(); i++ {
		var id int
		if err := rows.Scan(&id); err != nil {
			zap.L().Error("can't scan car id",
				zap.Int("id", i),
				zap.Error(err),
			)
		}
		cars[i].ID = id
	}
	return nil
}

func (r *carRepo) Update(ctx context.Context, car *model.Car) bool {
	dto := carToDTO(car)
	dtoVal := reflect.ValueOf(dto)
	carDtoType := reflect.TypeFor[carDTO]()
	fieldCount := reflect.Zero(carDtoType).NumField()
	updateSets := make([]string, 0, fieldCount-1)
	args := make([]any, 0, fieldCount-1)
	for i := 1; i < fieldCount; i++ {
		field := carDtoType.Field(i)
		colname := getDbTag(field)
		args = append(args, dtoVal.Field(i).Interface())
		updateSets = append(updateSets, fmt.Sprintf("%v = $%v", colname, len(args)))
	}
	args = append(args, dto.ID)
	query := fmt.Sprintf(
		"UPDATE cars SET %v WHERE id = $%v",
		strings.Join(updateSets, ", "),
		len(args),
	)
	zap.L().Debug("carRepo.Update quering",
		zap.String("query", query),
	)
	res, err := r.db.ExecContext(ctx, query, args...)
	if err != nil {
		zap.L().Error("carRepo.Update exec query failed",
			zap.Error(err),
		)
		return false
	}
	rowUpdated, err := res.RowsAffected()
	if err != nil {
		zap.L().Error("carRepo.Update: posgres driver does not support reports",
			zap.Error(err),
		)
	}
	if rowUpdated == 0 {
		return false
	}
	if rowUpdated > 1 {
		zap.L().Error("carRepo.Update: affects more than 1 row",
			zap.Int64("affected count", rowUpdated),
		)
	}
	return true
}

func (r *carRepo) Delete(ctx context.Context, id int) bool {
	const query = `DELETE FROM cars WHERE id = $1`
	zap.L().Debug("carRepo.Delete quering",
		zap.String("query", query),
	)
	res, err := r.db.ExecContext(ctx, query, id)
	if err != nil {
		zap.L().Error("query failed",
			zap.String("query", query),
			zap.Error(err),
		)
		return false
	}
	rowDeleted, err := res.RowsAffected()
	if err != nil {
		zap.L().Error("carRepo.Delete: posgres driver does not support reports",
			zap.Error(err),
		)
		return false
	}
	if rowDeleted == 0 {
		return false
	}
	if rowDeleted > 1 {
		zap.L().Error("carRepo.Delete: affects more than 1 row",
			zap.Int64("affected count", rowDeleted),
		)
	}
	return true
}

func NewCarRepository(postgres *Postgres) repo.CarRepository {
	return &carRepo{
		postgres,
	}
}
