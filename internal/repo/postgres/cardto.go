package postgres

import (
	"database/sql"
	"errors"

	"gitlab.com/triste/car-catalog/internal/model"
	"go.uber.org/zap"
)

type carDTO struct {
	ID              int            `db:"id"`
	RegNum          string         `db:"reg_num"`
	Mark            string         `db:"mark"`
	Model           string         `db:"model"`
	Year            sql.NullInt32  `db:"year"`
	OwnerName       string         `db:"owner_name"`
	OwnerSurname    string         `db:"owner_surname"`
	OwnerPatronymic sql.NullString `db:"owner_patronymic"`
}

func (c *carDTO) toModel() model.Car {
	return model.Car{
		ID:     c.ID,
		RegNum: c.RegNum,
		Mark:   c.Mark,
		Model:  c.Model,
		Year:   int(c.Year.Int32),
		Owner: model.CarOwner{
			Name:       c.OwnerName,
			Surname:    c.OwnerSurname,
			Patronymic: c.OwnerPatronymic.String,
		},
	}
}

func carToDTO(car *model.Car) carDTO {
	year := sql.NullInt32{}
	if car.Year != 0 {
		year.Int32 = int32(car.Year)
		year.Valid = true
	}
	patronymic := sql.NullString{}
	if len(car.Owner.Patronymic) > 0 {
		patronymic.String = car.Owner.Patronymic
		patronymic.Valid = true
	}
	return carDTO{
		ID:              car.ID,
		RegNum:          car.RegNum,
		Mark:            car.Mark,
		Model:           car.Model,
		Year:            year,
		OwnerName:       car.Owner.Name,
		OwnerSurname:    car.Owner.Surname,
		OwnerPatronymic: patronymic,
	}
}

type scanner interface {
	Scan(dest ...any) error
}

func newCarDtoFromScanner(s scanner) *carDTO {
	dto := carDTO{}
	err := s.Scan(&dto.ID, &dto.RegNum, &dto.Mark, &dto.Model, &dto.Year, &dto.OwnerName,
		&dto.OwnerSurname, &dto.OwnerPatronymic)
	if err != nil {
		if !errors.Is(err, sql.ErrNoRows) {
			zap.L().Error("GetByID scan row failed",
				zap.Error(err),
			)
		}
		return nil
	}
	return &dto
}
