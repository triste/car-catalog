package postgres

type Config struct {
	User     string `env:"USER" envDefault:"postgres"`
	Password string `env:"PASSWORD,required"`
	Host     string `env:"HOST,required"`
	Port     uint   `env:"PORT" envDefault:"5432"`
	Db       string `env:"DB"`
}
