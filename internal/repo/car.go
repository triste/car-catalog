package repo

import (
	"context"

	"gitlab.com/triste/car-catalog/internal/model"
)

type CarRepository interface {
	GetByID(ctx context.Context, id int) *model.Car
	GetByFilter(ctx context.Context, filter model.CarFilter) []model.Car
	Insert(ctx context.Context, cars []model.Car) error
	Update(ctx context.Context, car *model.Car) bool
	Delete(ctx context.Context, id int) bool
}
