package main

import (
	"os"
	"os/signal"
	"sync"
	"syscall"

	"github.com/caarlos0/env/v10"
	"gitlab.com/triste/car-catalog/internal/app"
	"go.uber.org/zap"
)

func init() {
	zap.ReplaceGlobals(zap.Must(zap.NewDevelopment()))
}

func main() {
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt, syscall.SIGTERM)
	envopts := env.Options{
		OnSet: func(tag string, val any, isdef bool) {
			if isdef {
				zap.L().Info("environment variable not set, using default",
					zap.String("name", tag),
					zap.Any("default", val),
				)
			}
		},
	}
	cfg := app.Config{}
	if err := env.ParseWithOptions(&cfg, envopts); err != nil {
		zap.L().Error("failed to parse environment variables",
			zap.Error(err),
		)
		return
	}
	app, err := app.NewApplication(cfg)
	if err != nil {
		zap.L().Error("failed to create application",
			zap.Any("config", cfg),
			zap.Error(err),
		)
		return
	}
	defer app.Close()
	wg := sync.WaitGroup{}
	wg.Add(1)
	go func() {
		if err := app.Start(); err != nil {
			zap.L().Error("application can't start or gracefully stop",
				zap.Error(err),
			)
		}
		wg.Done()
	}()
	s := <-sig
	zap.L().Info("signal recieved, stopping...",
		zap.Any("signal", s),
	)
	app.Stop()
	zap.L().Info("waiting for application goroutine...")
	wg.Wait()
}
