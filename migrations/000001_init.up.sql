BEGIN;

CREATE TABLE IF NOT EXISTS cars(
	id serial PRIMARY KEY,
	reg_num varchar CONSTRAINT cars_unique_reg_num UNIQUE NOT NULL,
	mark varchar NOT NULL,
	model varchar NOT NULL,
	year int,
	owner_name varchar NOT NULL,
	owner_surname varchar NOT NULL,
	owner_patronymic varchar
);

COMMIT;
